﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Secondary_File_System.lib;
using Secondary_File_System.Model;

namespace Secondary_File_System
{
    class Program
    {
        static FileSystem fileSystem;
        static char[] commandSeperater = { ' ' };
        static char[] paramSeperator = { ',' };
        static string[] authorityCommand = { "readonly", "readwrite" };
        static void Main(string[] args)
        {
            fileSystem = new FileSystem();
            string inputCommand, command;
            string[] param = new string[0];
            printPrefix();
            while ((inputCommand = Console.ReadLine().Trim()) != null)
            {
                string[] inputList = inputCommand.Split(commandSeperater, StringSplitOptions.RemoveEmptyEntries);
                command = inputList[0].ToLower();
                param = new string[0];
                if (inputList.Length > 1) param = inputList[1].Split(paramSeperator, StringSplitOptions.RemoveEmptyEntries);
                if (fileSystem.currentUser == null)
                {
                    if (command == "login")
                    {
                        if (param.Length < 2)
                        {
                            Console.WriteLine("参数不足！");
                            printPrefix();
                            continue;
                        }
                        if (!fileSystem.login(param[0].Trim(), param[1].Trim()))
                        {
                            Console.WriteLine("登录失败！");
                            printPrefix();
                            continue;
                        }
                        Console.WriteLine("登录成功！");
                        printPrefix();
                        continue;
                    }
                }
                switch (command)
                {
                    case "createuser":
                        if (param.Length < 2)
                        {
                            Console.WriteLine("参数不足！");
                            break;
                        }
                        if (fileSystem.createUser(param[0].Trim(), param[1].Trim())) Console.WriteLine("创建用户成功！");
                        else Console.WriteLine("创建用户失败！");
                        break;
                    case "info":
                        fileSystem.info();
                        break;
                    case "dir":
                        fileSystem.dir();
                        break;
                    case "create":
                        if (param.Length < 1)
                        {
                            Console.WriteLine("参数不足！");
                            break;
                        }
                        if (fileSystem.createFile(param[0].Trim(), false)) Console.WriteLine("创建文件成功！");
                        else Console.WriteLine("创建文件失败！");
                        break;
                    case "delete":
                        if (param.Length < 1)
                        {
                            Console.WriteLine("参数不足！");
                            break;
                        }
                        if (fileSystem.deleteFile(param[0].Trim())) Console.WriteLine("删除文件成功！");
                        else Console.WriteLine("删除文件失败！");
                        break;
                    case "open":
                        if (fileSystem.editingFile)
                        {
                            Console.WriteLine("已经有正在编辑的文件，无法再次打开一个文件！");
                            break;
                        }
                        if (param.Length < 1)
                        {
                            Console.WriteLine("参数不足！");
                            break;
                        }
                        if (fileSystem.openFile(param[0].Trim())) Console.WriteLine("打开文件成功！");
                        else Console.WriteLine("打开文件失败！");
                        break;
                    case "close":
                        if (!fileSystem.editingFile)
                        {
                            Console.WriteLine("没有已打开的文件！");
                            break;
                        }
                        if (fileSystem.closeFile()) Console.WriteLine("关闭文件成功！");
                        else Console.WriteLine("关闭文件失败！");
                        break;
                    case "read":
                        if (!fileSystem.editingFile)
                        {
                            Console.WriteLine("没有已打开的文件！");
                            break;
                        }
                        Console.WriteLine(fileSystem.readFile());
                        break;
                    case "write":
                        if (!fileSystem.editingFile)
                        {
                            Console.WriteLine("没有已打开的文件！");
                            break;
                        }
                        if (param.Length < 1)
                        {
                            Console.WriteLine("参数不足！");
                            break;
                        }
                        if (fileSystem.writeFile(param[0].Trim())) Console.WriteLine("写入文件成功！");
                        else Console.WriteLine("写入文件失败！");
                        break;
                    case "changeauthority":
                        if (!fileSystem.editingFile)
                        {
                            Console.WriteLine("没有已打开的文件！");
                            break;
                        }
                        if (param.Length < 1)
                        {
                            Console.WriteLine("参数不足！");
                            break;
                        }
                        if (!Utility.inArray(param[0].Trim(), authorityCommand))
                        {
                            Console.WriteLine("参数错误！");
                            break;
                        }
                        bool readOnly = param[0].Trim() == authorityCommand[0];
                        if (fileSystem.changeAuthority(readOnly)) Console.WriteLine("更改文件权限成功！");
                        else Console.WriteLine("更改文件权限失败！");
                        break;
                    case "copy":
                        if (param.Length < 2)
                        {
                            Console.WriteLine("参数不足！");
                            break;
                        }
                        if (fileSystem.copyFile(param[0].Trim(), param[1].Trim())) Console.WriteLine("复制文件成功！");
                        else Console.WriteLine("复制文件失败！");
                        break;
                    case "exit":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("指令错误");
                        break;
                }
                printPrefix();
            }
        }
        static void printPrefix()
        {
            string currentUser = fileSystem.currentUser == null ? "未登录" : fileSystem.currentUser.username;
            Console.Write("[{0}#]:", currentUser);
        }
    }
}
