﻿using Secondary_File_System.lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secondary_File_System.Model
{
    /// <summary>
    /// 超级块
    /// </summary>
    class SuperBlock
    {
        public int freeUser;//空闲用户数
        public int freeDirectory;//空闲目录数
        public int freeInode;//空闲inode的数
        public int freeBlock;//空闲块的数
        public SuperBlock()
        {
            this.freeUser = FileSystem.userCount;
            this.freeDirectory = FileSystem.directoryCount;
            this.freeInode = FileSystem.inodeCount;
            this.freeBlock = FileSystem.blockCount;
        }
        public static SuperBlock readFromStream(ref FileStream fs)
        {
            SuperBlock super = new SuperBlock();
            super.freeUser = Utility.readIntFromStream(ref fs);
            super.freeDirectory = Utility.readIntFromStream(ref fs);
            super.freeInode = Utility.readIntFromStream(ref fs);
            super.freeBlock = Utility.readIntFromStream(ref fs);
            long remainingBytes = 1 * FileSystem.K - sizeof(int) * 4;
            fs.Seek(remainingBytes, SeekOrigin.Current);
            return super;
        }
        public void save(ref FileStream fs)
        {
            //freeUser
            Utility.writeBytesToStream(BitConverter.GetBytes(this.freeUser), ref fs);
            //freeDirectory
            Utility.writeBytesToStream(BitConverter.GetBytes(this.freeDirectory), ref fs);
            //freeInode
            Utility.writeBytesToStream(BitConverter.GetBytes(this.freeInode), ref fs);
            //freeBlock
            Utility.writeBytesToStream(BitConverter.GetBytes(this.freeBlock), ref fs);
            long remainingBytes = 1 * FileSystem.K - sizeof(int) * 4;
            fs.Seek(remainingBytes - 1, SeekOrigin.Current);
            fs.WriteByte(new byte());
            fs.Flush();
        }
    }
}
