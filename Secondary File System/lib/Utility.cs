﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secondary_File_System.lib
{
    class Utility
    {
        public static void writeBytesToStream(byte[] bytes, ref FileStream fs)
        {
            fs.Write(bytes, 0, bytes.Length);
        }
        public static int readIntFromStream(ref FileStream fs)
        {
            byte[] buffer = new byte[sizeof(int)];
            fs.Read(buffer, 0, sizeof(int));
            return BitConverter.ToInt32(buffer, 0);
        }
        public static bool readBoolFromStream(ref FileStream fs)
        {
            byte[] buffer = new byte[sizeof(bool)];
            fs.Read(buffer, 0, sizeof(bool));
            return BitConverter.ToBoolean(buffer, 0);
        }
        public static string readStringFromStream(ref FileStream fs, int bytesLimit)
        {
            byte[] buffer = new byte[bytesLimit];
            fs.Read(buffer, 0, bytesLimit);
            return FileSystem.systemEncoding.GetString(buffer).Trim();
        }
        public static bool inArray(string @object, string[] array)
        {
            foreach (string target in array)
                if (@object == target) return true;
            return false;
        }
    }
}
